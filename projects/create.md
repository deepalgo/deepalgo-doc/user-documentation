# Create a project

Work in Deep Algo is done within a project. A project is where you publish your documentation.

## Overview
To create a project in Deep Algo, click either the “Create your first project here” link in the first dashboard or “NEW PROJECT” in the title bar to create your first project link.

- [The new project](#the-new-project-page) page

- [Go Back to Deep Algo Docs Home](README.md)

# The New project page

Following information must filled: 

- The **name** of your project in the Project name field. You can’t use special characters, but you can use spaces, hyphens, underscores or even emoji.

- The **Organization**: With Deep Algo Organizations, you can:
  * Assemble related projects together.
  * Grant members access to several projects at once.
You can read details about documentation in the Organization Menu section.
- Pick the sources files **to match your billing plan restrictions** (see [your profile](profile/profile.md))
- When your repository is **private**, **activate the toggle** and connect Deep Algo to your Git Source control by registering a **Personal Access Token**.

You’ll find explanations how to create Personal Access Token for Gitlab, Github or Bitbucket on following the links below:

| ![Github](img/github_logo.png)   | ![Gitlab](img/gitlab-logo.png)  |![bitbucket](img/bitbucket-logo.svg)|
|:------------------------|:-----------------------------------------|------------------------------------|

- [GitLab Help](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) | Personal access tokens
- [GitHub Help](https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line) | Creating a personal access token for the command line
- [Bitbucket Help](https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html) | Personal access tokens

- **Git Source** :

The **Git URL** of the source code’s repository you want to document in the Git Source field.

> **TIP:** It must be a 'Clone with https://''. The format of the Git URL is as
follows : 'https://gitlab.com/deepalgolab/hello-world.git'.

The **Source Branch Name** you want the documentation to be created from


The **error message** can be due to:

- A wrong Git URL (URL format error, private repository...)
- A problem with your authentication token
- A lack of privilege in your subscription plan. For example, you might not be able to document a project with too many lines of code. In this case, Deep Algo will suggest you to upgrade your plan.

