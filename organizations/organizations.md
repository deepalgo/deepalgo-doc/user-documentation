# Manage your organizations

## Overview

- [What's an organization?](#what-s-an-organization)
- [Create an organization?](#create-an-organization)
- [Change the Organization](#change-the-organization)
- [Add users to an organization](#add-users-to-an-organization)
- [Go Back to Deep Algo Docs Home](README.md)

## What's an organization?

With Deep Algo Organization, you can:
- assemble related projects together
- grant members access to several projects at once

You can access the organization management by clicking on your profile in the
upper right corner.

![profile-menu](img/profile-menu.png)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Create an organization

To create a new organization, click your `Profile` and then `Organization`
in the top menu.

![create-organization](img/create-organization.png)

1. Enter the name of the new organization. This is the name that displays in
organization views.
2. Click on the `CREATE ORGANIZATION` button.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Change the Organization

If you want to get the list of project from a specific organizations, you only
 need to jump to the targeted organization using the dedicated menu in
the menu bar.

![organizations](img/jump-organizations.png)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## Add users to an organization

A benefit of putting multiple projects in one organization is that you can give
a user access to all projects in the organization with one action.

To add members in an organization, click your `Profile` and then `Organization`
in the top menu.

![create-organization](img/create-organization.png)

Click on the `Ìnvite User` button ![invite-user](img/invite-user.png)

And complete the following information:

![invite-user-2](img/invite-user-2.png)

Then, Click on the `INVITE` button.

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
