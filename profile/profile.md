# Manage your profile

## Overview

- [What's a profile?](#what-s-a-profile)
- [The User profile](#the-user-profile)
- [Go Back to Deep Algo Docs Home](README.md)

## What's a profile?

Each Deep Algo account has a user profile, and settings. Your profile contains
information about you

You create a Deep Algo account by
[signing up to the application](connect/connect.md).

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

## The User profile

To access your profile:
1.  click on your avatar in the upper right corner of the top bar.
2. Select **Profile**

On your profile page, you will see the following information:

- Personal information
- Your authentication token
- Your subscription plan
If you need more information on subscription plans, please visit our
website (https://www.deepalgo.com)
- All organizations you belong to.
  - If you do not belong to any organization you can :
    1. Create one yourself and invite your mates (you will be the owner)
    2. Ask your mates to invite you withing an existing organization (you will be master)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
