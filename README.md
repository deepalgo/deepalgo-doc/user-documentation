# What is [Deep Algo](https://www.deepalgo.com/)?

As a developer, incomplete or outdated documentation is a pervasive problem.

Deep Algo is SaaS platform that **automatically** generates the documentation of your code.

## Quick Start with our Hello World!

Let's start the [Hello World](hello/hello-world.md) tutorial!


## Prerequisites
- Your source code is managed by a Git source control like Gitlab, Github or Bitbucket. We just need the Git URL and the read access credentials.

- Our **supported Web browsers**
Connect to Deep Algo from the web on your desktop anytime at https://app.deepalgo.com

> | Browser   | Requirements         |
> |:----------|----------------------|
> | Chrome    | Version 79 or above  |
> | Chromium  | Version 63 or above  |
> | Firefox   | Latest               |

- Unsupported browsers
To focus on delivering the best possible experience in Deep Algo, it is necessary
to keep the list of supported browsers short.

When a browser is no longer supported, we stop fixing pesky bugs and issues.

- **Mobile browsers**
We're working hard to have the solution working on mobile and tablets, but as
far as now we do not support the use on mobile because the user experience is
not good. We'll keep you inform as soon as this feature is available.



## Use cases
- document as a code (Docs as Code) in english : add `// @deepalgo` comment in your code and get the documentation (see [guidelines](ci-cd/guidelines-add-comment.md)) 
- integrate the documentation workflow in your DevOps toolchain
- keep this documentation updated by integrating this workflow in your CI
- get an interactive documentation to simulate outcomes depending on selected conditions

## Get deeper
Following is the overall workflow:
- [Connect](connect/connect.md) the Deep Algo platform (https://app.deepalgo.com)
- [Create](projects/create.md) a project
- [Launch](pipelines/pipelines.md) a documentation
- [Read](docs/docs.md) a documentation
- [Automate your documentation](ci-cd/ci-cd.md)

## The project page

The project page contains different menu to manage :
- [Settings](settings/settings.md) Menu
- [Docs](docs/docs.md) Menu ([Docs as Code](docs/docs.md#doc-as-code), [Interactive doc](docs/docs.md#interactive-doc), [Doc on demand](docs/docs.md#doc-on-demand))
- [Pipelines](pipelines/pipelines.md) Menu
- [CI/CD](ci-cd/ci-cd.md) Menu

## Manage your profile

Learn how to [manage your profile](profile/profile.md).

# Support and Help Center

We use Intercom Chat both to enable real-time communication with our support team and fix your issues as soon as possible.

![Intercom](img/intercom_chatbot.png)

Feel free to contact us by clicking the Chatbot at the right bottom of your browser.

# Subscription plan

To get all information about our subscription plan, feel free to visit our website (https://www.deepalgo.com).

# What's new?

#### 20.1.8
What an easy question! Everything is new!  Enjoy!

The last version is 20.1.8. You can see the version you use by clicking on the
information icon ![information-icon](img/information-icon.png).

# What's next?

What about the next development of Deep Algo?

Help us to develop the most valuable features.

![Intercom](img/intercom_chatbot.png)

Give us some feedbacks using the Chatbot at the bottom right corner.

Some ideas:
- C# language
- ….

# Frequently Asked Questions (FAQ)
## How does it work?
We have replicated the steps a developer would do if he had to produce a documentation: 

 1. Reverse engineer the code base to get a universal representation in our own language : Unified Meta Model
 2. Analyse the parsed code at a technical level: who's calling who? data flow, etc ...
 3. Make it understandable: draw simple diagrams, summarize in english and write  down the major results as simple formuas
 
## What can we understand and therefore document?
- As of today, we can document **JAVA** code only. We are beta testing **C#**.
- We can document pieces of code where a `@deepalgo` comment lies. To understand our **DocAsCode** principles visit the [guidelines](ci-cd/guidelines-add-comment.md).
- We are capable to document object oriented programming. Therefore not only variables with intrinsec types are documentable but also complex data structures. 
- We inline all methods where they are called. That is to say: in the following exemple: 

  ```java
  /// file Main.java
  public class Main {
      // @deepalgo
      public static int main(String[] argv){
        Foo f = new Foo(argv[0]);
        return f.run()
      }
  }
  /// file Foo.java
  public class Foo{
    private String name;
    public Foo(String name){
      this.name = name;
    }
    public int run(){
      // a butifull code that itself delegates to 
      // other pieces of code
    }
  }
  ```
  You only need to tag the `main` method with `// @deepalgo` comment, we will dive in the algorithm to understand what it really does.

- We understand loops: `for`, `while` and `forEach`, even accross inlined methods 
- We understand native arrays: `Foo[] listOfFoo`
- We partially handle non native arrays such as `List` or `HashMap` with following limitations: 
  * Only for **JAVA**
  * Only for the followig list of operations `add, addAll, remove, removeAll, sort, clear, put, putAll, putIfAbsent, replace, replaceAll`
- We handle recursive calls: for instance a recursive implementation of the `factorial(n)` function would lead to : `factorial(n) = (n * (n-1)*factorial(n-1-1))`
  * The documentation may fail or timeout if the recursion gets too complex : multiple recursive calls inside each other
- We handle conditions by providing an interactive logical flow graph. The conditions are gathered accross all inlined methods.

## What can't we understand... and therefore can't document?
- We cannot analyse the external components of your code (because we can't analyse the code we don’t have :-) ). Actually this is not a real limitation... indeed let's take this piece of code:

```c++
#include "iostream"
int main(int argc, char * [] argv){
  printf("Hello world!");
  return 0;
}
```

If we were to explain what `printf` does, we would ultimately end up into saying that: "the application shifts a bunch of buffers" ... which is not false, but totaly useless.

- So far output parameters are not handled : 
 ```java
  /// file Main.java
  public class Main {
      // @deepalgo
      public static int main(String[] argv){
        Foo f = new Foo(argv[0]);
        String someVariable = "";
        f.run(someVariable);
        System.out.println(someVariable);
        return 0
      }
  }
  /// file Foo.java
  public class Foo{
    private String name;
    public Foo(String name){
      this.name = name;
    }
    public void run(String outParam){
      outParam = "Hellow world";
    }
  }
  ```
The `someVariable` will not be replaced by the value `"Hellow world"`

- Some methods are hard to properly inline. Indeed in the very simple explemple below:
```java
  /// file Main.java
  public class Main {
      // @deepalgo
      public static int main(String[] argv){
        Foo f = new Foo(argv[0]);        
        f.run(argv.length);        
        return 0
      }
  }
  /// file Foo.java
  public class Foo{
    private String name;
    public Foo(String name){
      this.name = name;
    }
    public Double run(Double p){
      return p * this.name.size();
    }
    public Double run(String p){
      return p.size() * this.name.size();
    }
  }
  ```
The call to `run` is ambiguous for us... `argv.length` is not known to Deep Algo, therefore we cannot infer the type of `length` (it seam obvious for a human ... but it is not for a machine). Since it exists 2 implementations of the `run` we may inline the first one or the last one.

- Licenses: There is great to deal with this pain on the market!

- Class Diagrams: we’re sure you can find that in your IDE.

- Your Code of conduct :-)

## It seams that my result is not fully inlined?
This case may rise when the call is not linked to any method though it should be. Most probably a lack of context or a real bug on our side.
When loops are involved, as follows (for instance):

```java
  /// file Main.java
  public class Main {
      // @deepalgo
      public static int main(String[] argv){
        Foo f = new Foo(argv[0]);
        // @deepalgo
        Double res = 1.0;
        for(/* some arguments here */){
          res = f.run(res * i)
        }
        return 0
      }
  }
  /// file Foo.java
  public class Foo{
    private String name;
    public Foo(String name){
      this.name = name;
    }
    public Double run(Double p){
      return p * this.name.size();
    }
  }
  ```
The final result 

## Why is my documentation empty?
Mostlikely because there is nothing relevant to document. Normally, Deep Algo tries to avoid such irrelevant methods, to reduce noise, but sometimes the code base itself is confusing

## Why results of a keyword in the search bar do not involve the keyword itself?
Deep Algo indexes your documented methods by taking into account the fully inlined algorithm.  Meaning that the keyword may be very relevant (and therefore pop in the results) though it appears much deeper than in the method itself.

## Which languages can be analyzed?
Deep Algo is language agnostic: it's independent of any specific programming
language. We only need to implement the grammar of the language.

Deep Algo can currently manage applications in **Java**.

We're working hard to add new languages.

## How long does an analysis last?
Deep Algo is able to analyze 50k lines of a java code in around 3 hours, yet this may vary drastically depending on your code.

## What's your confidence level in an analysis?
For each analysis, we get an understanding indicator with a detailed report of
all the files, lines of code Deep Algo didn't understand.

# Known issues
Despite the aforementioned limitations, we know we have some issues, and we are already working hard on it: 
- In the final documentation the sentence "list of MyClass" appears frequently, yet no array or list are involved.

All the known issues are gathered as an issue just here: 
https://gitlab.com/deepalgo/deepalgo-doc/known-issues/-/issues

Feel free to add any issue you could identifiy. Thks in advance for your collaboration !






