# Docs menu

## Overview
Deep algo generates:

- A [README](docs/docs.md#readme),
- A set of [interactive doc](docs/docs.md#interactive-doc),
- A [search](docs/docs.md#search)
- [Go Back to Deep Algo Docs Home](README.md)

## README

The `README.md` lists all documented methods in addition to found constants and thrown errors. 

Methods come with an english summary

![Sample readme](img/readme.png)



## Interactive doc

This is the entire documentation of a method or a variable. It is composed of several parts described bellow.

![Sample documentation](img/doc-part1.png)



![Sample documentation](img/doc-part2.png)




![Documentation toolbox](img/doc-part3.png)

## Search

Search your documentation by keyword. Interesting enough, keywords indexing will also follow inlining: when an algorithm is spread over many sub methods, the very important keyword may lie in those inlined methods and not necessarily in the root one. Deep Algo indexes the important ones.

![Search doc](img/search.png)
