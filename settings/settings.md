# Settings menu

## Overview
- [What is it about?](#what-is-it-about)
- [Go Back to Deep Algo Docs Home](README.md)


## What is it about?
In the settings menu, you get all the information relating your project.

![Settings Menu](img/settings-menu.png)

As of today, it's impossible to change the content of this page.
All of this was entered by when you [created you project](projects/create.md).

In a future version of Deep Algo, for sure, it'll be possible to change it.
Tell us if your think it should be a top priority!

Use the Intercom chatbot: ![Intercom](img/intercom_chatbot.png)

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Quick Access <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
