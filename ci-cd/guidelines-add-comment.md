# Guidelines to add `@deepalgo` comment

- **On method returning something**.

'void' methods are not "explainable" by themselves since they may affect many different objects or variables in the code. By tagging non-void method, the documentation will concern the returned value and the return values only, even if this method also impacts other variables.
The `@deepalgo` comment must be set in a comment block **preceding the method**.

Java example: 

```java
package my.org.app;

class ExempleOnReturnMethod {
    /* Your comments here
     * @deepalgo
     *
     * @param input[double] some information
     * @return SomeClass some other information
     */
    SomeClass run(double input){
        return new SomeClass();
    }
}
```
​
- On a **specific variable**. 

The `@deepalgo` comment must be set in a comment block **preceding the variable**.
In the following example, the variable 'object' will be documented. That is to say all its states will be described.

Java exemple:

```java
package my.org.app;

class ExempleOnVariable {

    void run(){
        // @deepalgo
        SomeClass object = new SomeClass();
        /*
         *  your code goes here
         */
    }
}
```
​
- **Using polymorphism**.

One powerful aspect of our technology comes with the overloading / overwriting management. To document all implementations of a methods defined in an interface, abstract class and simple class, only one `@deepalgo` comment has to be set on the method belonging highest class in the hierarchy.

The `@deepalgo` comment must be set in a comment block **preceding the method declaration**. In the following example, all specific implementations will be documented in a unique file.


Java exemple:

```java
package my.org.app;

interface YourInterface {
    // @deepalgo
    SomeResult run();
}


class FirstImplem implements YourInterface {
    SomeResult run(){
    }
}
class SecondImplem implements YourInterface {
    SomeResult run(){
    }
}

class ThirdImplem extends FirstImplem {
    SomeResult run(){
    }
}

```

- **Anotations**.

When your method is preceeded with anotations (such as `@Override`) make sure to put the `@deepalgo` **BEFORE** the anotation
