# Quick Start with our Hello World

The **Hello World** project is a time-honored tradition in computer programming.
It is a simple exercise that gets you started when learning something new.

Let's get started with Deep Algo!

## Follow these steps
1. Click on the `NEW PROJECT` button.
2. Load your repo by filling the URL with following address: https://gitlab.com/deepalgolab/hello-world.git
  2.1 Click the refresh button
3. On the left panel, enter the name you want for your project
4. Click the play button on the right hand side
5. Click the rotating refresh button to see advancement.
6. Once it is all green you can click the `README` menu and see documented methods
7. Click the link `String Global.hello.Greeter.sayHello()`
8. You have a full documentation of the famous **Hello World**

## What can you do from now

 * You can play with the outcome of the `sayHello()` method:
   1. Click the red `lock` button to get edition rights
   2. Move to the *Logical flow** section and select the red branch in the logigram 
   3. Click the update button
   4. You now should see `(Hello + name + !)` instead of simply `Hello!`

 * You can create an organisation and invite your contributors to share the documentation of your project by managing your [profile](profile/profile.md)
  

## Celebrate

By completing this tutorial, you’ve learned to create a project and make a new
documentation on Deep Algo!

Here’s what you accomplished in this tutorial:

- Created a new project
- Started an analysis
- Opened the documentation and play with it

> **TIP:** To learn more about the power of Deep Algo, we recommend reading the [Deep Algo Docs Home](README.md).
