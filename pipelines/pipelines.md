# Pipelines : Manage your documentation

## Overview
- [Pipelines Menu](#pipelines-menu)
- [Pipelines Logs (for support purposes)](#pipelines-log-for-support-purposes)
- [Go Back to Deep Algo Docs Home](README.md)

## Pipelines Menu

This section will let your monitor the advancement of the documentation generation process. 
This page will automatically be displayed after you create a new project.

![Pipelines Menu](img/pipeline-menu.png)

Deep algo documents only what you want to document.
You only need to add a comment `// @deepalgo` right above the method or the variable you want to document.

> Tips: in your project settings your can toggle `All non void method` : this will automatically document all non `void` methods in your code base assuming they are *relevant enough* (basically `getters` will be excluded).
> Look at the [Guidelines to add comments](ci-cd/guidelines-add-comment.md)
> If you flag a virtual method of an abstract class or an interface, all Use Cases of its implementation will be documented.


When you’re done tagging all your methods, you can click the Launch New version button in the upper right corner.
![New Version Button](img/new-version-button.png)

All steps and logs of the technical analysis performed by Deep Algo are reported in [Pipeline Logs (for support purposes)](#pipelines-log-for-support-purposes)

When the pipeline is done (the status is green), you can start consulting the documentation in the [docs menu](docs/docs.md)

In case of an error, both you and our support team will be notified to clear the issue.

Depending on the size of your project, the analysis can last from a couple of minutes to a couple of hours.


## Pipeline Logs (for support purposes)

![Pipeline Logs](img/pipeline-menu-log.png)

You one of the step is **red**, please send us the logs in Intercom chatbot ![Intercom](img/intercom_chatbot.png).
You can easily copy/paste the logs using this icon ![copy-paste-log](img/copy-paste-log.png).
